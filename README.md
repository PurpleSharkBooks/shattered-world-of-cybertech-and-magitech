# Shattered World: Of Cybertech and Magitech

A role-playing game exploring the future after the end of capital.

## Building

### Requirements

To build Shattered World you need a copy of the source (either via cloning
this repository or downloading an archive of the source), the .Net Core 2 SDK,
and optionally a (free, with some resitrictions) copy of Visual Studio 2019
Community for Windows or Mac.

### Building from the command line

Step by step:
1. `git clone https://gitlab.com/PurpleSharkBooks/shattered-world-of-cybertech-and-magitech.git`
2. `cd shattered-world-of-cybertech-and-magitech`
3. `dotnet build` if you want to build the binaries, `dotnet restore` if you
want to pull in the NuGet packages SW depends on and hack away in your favorite IDE.

### Using Visual Studio 2019

From the Visual Studio start window when you start VS2019, select "Clone or
checkout code" and paste the HTTPS repository URL in the top most box, pick a
spot for this repository to live, and VS2019 will do most of the work for you.

You can open the solution file from within the VS2019 soluton explorer.

#### Troubleshooting

If Visual Studio didn't restore the NuGet packages, you can do so by right
clicking on the solution once it's open, and selecting "Restore NuGet packages."

If the project doesn't build from within Visual Studio, you need to right click
on the ShatteredWorldCore project, and add a reference to Monogame
(Add -> Reference).
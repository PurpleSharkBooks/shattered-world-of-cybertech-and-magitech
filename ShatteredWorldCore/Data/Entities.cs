﻿using ShatteredWorld.Defines.Components;
using System.Collections.Generic;

namespace ShatteredWorld.Data
{
    public static class Entities
    {
        private static int chargenPoints = 10;
        public static Dictionary<string, TraitComponent> Traits = new Dictionary<string, TraitComponent>();
        public static int GetChargenPoints()
        {
            return chargenPoints;
        }
        public static void SetChargenPoints(int value)
        {
            chargenPoints = value;
        }
    }
}

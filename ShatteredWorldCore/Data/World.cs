﻿using ShatteredWorld.Defines;
using ShatteredWorld.Systems;
using System;

namespace ShatteredWorld.Data
{
    public static class World
    {
        private static int mapWidth = 100;
        private static int mapHeight = 100;
        private static int overworldMapWidth = 100;
        private static int overworldMapHeight = 100;
        private static int seed = 1000;
        private static SWEntity player;
        public static OverworldMap OverworldMap { get; set; }
        public static SWEntity GetPlayer()
        {
            return player;
        }
        public static void SetPlayer(SWEntity value)
        {
            player = value;
        }
        public static int GetMapWidth()
        {
            return mapWidth;
        }
        public static void SetMapWidth(int value)
        {
            mapWidth = value;
        }
        public static int GetOverworldMapWidth()
        {
            return overworldMapWidth;
        }
        public static void SetOverWorldMapWidth(int value)
        {
            overworldMapWidth = value;
        }
        public static int GetOverworldMapHeight()
        {
            return overworldMapHeight;
        }
        public static void SetOverWorldMapHeight(int value)
        {
            overworldMapWidth = value;
        }
        public static int GetMapHeight()
        {
            return mapHeight;
        }
        public static void SetMapHeight(int value)
        {
            mapHeight = value;
        }
        public static int GetSeed()
        {
            return seed;
        }
        public static void SetSeed(int value)
        {
            seed = value;
        }
        public static Random GetRandomGen(int seed = 0)
        {
            return new Random(seed);
        }
    }
}

﻿using SadConsole;
using ShatteredWorld.UI.Views;
using System;

namespace ShatteredWorld.Systems
{
    public class UserInterface : ContainerConsole
    {
        private readonly int _mapWidth;
        private readonly int _mapHeight;
        private readonly int _screenWidth;
        private readonly int _screenHeight;
        public StartScreen StartScreen { get; private set; }
        public UserInterface(int screenWidth, int screenHeight, int mapWidth, int mapHeight)
        {
            _screenHeight = screenHeight;
            _screenWidth = screenWidth;
            _mapWidth = mapWidth;
            _mapHeight = mapHeight;
            IsVisible = true;
            IsFocused = true;
            StartScreen = new StartScreen(_screenWidth, _screenHeight);
        }
        public override void Update(TimeSpan timeElapsed)
        {
            base.Update(timeElapsed);
        }

        // Reference maths, so keep for now
        private static void CalculateSizes(int newWindowWidth, int newWindowHeight, out int mapHeight, out int infoHeight, out int mapWidth, out int infoWidth)
        {
            mapHeight = (int)Math.Floor(Math.Abs(newWindowHeight * 0.8f));
            infoHeight = newWindowHeight - mapHeight;
            mapWidth = newWindowWidth;
            infoWidth = (int)Math.Floor(Math.Abs(newWindowWidth * 0.5f));
        }
    }
}

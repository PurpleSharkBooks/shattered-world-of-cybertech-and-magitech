﻿using Microsoft.Xna.Framework;
using SadConsole;
using SadConsole.Controls;
using ShatteredWorld.Defines;
using ShatteredWorld.Utils;
using System;
using System.Collections.Generic;

namespace ShatteredWorld.UI.Views
{
    public class StartScreen : ContainerConsole
    {
        internal Button mapgenButton;
        internal Button chargenButton;
        internal Button startGameButton;
        internal Button closeCGWindowButton;
        internal Button closeMGWindowButton;
        internal Button closeLicenseWindowButton;
        internal Button saveCharButton;
        internal Button licenseButton;
        internal Button quitGameButton;
        internal Window cgWindow;
        internal Window mgWindow;
        internal Window licenseWindow;
        internal int _screenWidth;
        internal int _screenHeight;
        internal int _currentPoints;
        private List<string> selectedComponents = new List<string>();
        internal string description = "";
        internal List<CheckBox> listCBs = new List<CheckBox>();
        private string text = "";
        public ControlsConsole MenuView { get; private set; }
        public StartScreen(int screenWidth, int screenHeight)
        {
            _screenWidth = screenWidth;
            _screenHeight = screenHeight;
            _currentPoints = Data.Entities.GetChargenPoints();
            UseMouse = true;
            var bgTxt = new CellSurface(80, 60);
            var ansiTitle = new SadConsole.Ansi.AnsiWriter(FileLoader.TitleScreen, bgTxt);
            ansiTitle.ReadEntireDocument();
            ansiTitle.Process(0);
            SetupViews(screenWidth, screenHeight, bgTxt);
            SetupTraitCheckBoxes();
            RegisterEvents();
        }
        public override void Update(TimeSpan timeElapsed)
        {
            if (cgWindow != null)
            {
                PrintCharGenWindowTitle();
                foreach (var item in cgWindow.Controls)
                {
                    TraitDescription(item);
                }
            }
            base.Update(timeElapsed);
        }
        private void SetupTraitCheckBoxes()
        {
            int i = 2;
            int j = 2;
            foreach (var item in Data.Entities.Traits)
            {
                if (item.Value.Category != "Kind")
                {
                    var cb = new CheckBox(30, 1)
                    {
                        Position = new Point(i, j),
                        Text = item.Value.Name,
                        Name = item.Key
                    };
                    listCBs.Add(cb);
                    j++;
                }
            }
        }
        private void SetupViews(int screenWidth, int screenHeight, CellSurface title)
        {
            mapgenButton = UIFactory.CreateButton(15, "New World", 27, 13);
            chargenButton = UIFactory.CreateButton(15, "New Character", 35, 15);
            startGameButton = UIFactory.CreateButton(15, "Start game", 43, 17);
            licenseButton = UIFactory.CreateButton(15, "License", 45, 13);
            quitGameButton = UIFactory.CreateButton(15, "Quit", 27, 17);
            closeCGWindowButton = UIFactory.CreateButton(1, "x", 0, 0);
            closeMGWindowButton = UIFactory.CreateButton(1, "x", 0, 0);
            closeLicenseWindowButton = UIFactory.CreateButton(1, "x", 0, 0);
            MenuView = new ControlsConsole(screenWidth, screenHeight);
            MenuView.Add(chargenButton);
            MenuView.Add(mapgenButton);
            MenuView.Add(startGameButton);
            MenuView.Add(licenseButton);
            MenuView.Add(quitGameButton);
            MenuView.UseMouse = true;
            Children.Add(MenuView);
            MenuView.SetSurface(title);
        }
        private void RegisterEvents()
        {
            chargenButton.Click += ChargenButton_Click;
            mapgenButton.Click += MapgenButton_Click;
            startGameButton.Click += StartGameButton_Click;
            licenseButton.Click += LicenseButton_Click;
            quitGameButton.Click += QuitGameButton_Click;
            closeLicenseWindowButton.Click += CloselicenseWindowButton_Click;
            closeCGWindowButton.Click += ClosecgwindowButton_Click;
            closeMGWindowButton.Click += ClosemgwindowButton_Click;
        }
        private void TraitDescription(object item)
        {
            if (item is CheckBox s)
            {
                if ((s.State == (ControlStates.Focused | ControlStates.MouseOver))
                    || (s.State == ControlStates.Focused)
                    || (s.State == ControlStates.MouseOver))
                {
                    BlankText(2, _screenHeight - 5, text);
                    BlankText(2, _screenHeight - 4, description);
                    description = Data.Entities.Traits[s.Name].Description;
                    text = $"Description for {s.Name} [Cost: {Data.Entities.Traits[s.Name].Cost}] in {Data.Entities.Traits[s.Name].Category}";
                    cgWindow.Print(2, _screenHeight - 5, text);
                    cgWindow.Print(2, _screenHeight - 4, description);
                }
            }
        }
        private void BlankText(int x, int y, string text)
        {
            if (text.Length > 0)
            {
                string blank = " ";
                for (int i = 0; i < text.Length; i++)
                {
                    blank += " ";
                }
                cgWindow.Print(x, y, blank);
            }
        }
        private void PrintCharGenWindowTitle()
        {
            string winTitle = $"Character Generation (Points available: {_currentPoints})";
            cgWindow.Title = winTitle.Align(HorizontalAlignment.Center, cgWindow.Width);
        }
        #region Events
        private void Item_IsSelectedChanged(object sender, EventArgs e)
        {
            int cost;
            TraitDescription(sender);
            if (sender is CheckBox s)
            {
                if (s.IsSelected == true)
                {
                    cost = Data.Entities.Traits[s.Name].Cost * -1;
                    selectedComponents.Add(s.Name);
                }
                else
                {
                    cost = Data.Entities.Traits[s.Name].Cost;
                }
                if (s.IsSelected == false)
                    selectedComponents.Remove(s.Name);
                _currentPoints += cost;
            }
        }
        private void ClosecgwindowButton_Click(object sender, EventArgs e)
        {
            var _s = sender as Button;
            _s.Parent.IsVisible = false;
            cgWindow = null; // hacky wiping of state, needs rebuilding
        }
        private void ClosemgwindowButton_Click(object sender, EventArgs e)
        {
            var _s = sender as Button;
            _s.Parent.IsVisible = false;
        }
        private void CloselicenseWindowButton_Click(object sender, EventArgs e)
        {
            var _s = sender as Button;
            _s.Parent.IsVisible = false;
        }
        private void LicenseButton_Click(object sender, EventArgs e)
        {
            if (licenseWindow == null)
            {
                licenseWindow = UIFactory.CreateWindow(_screenWidth - 2, _screenHeight - 2, 1, 1);
                string winTitle = "License";
                licenseWindow.Title = winTitle.Align(HorizontalAlignment.Center, licenseWindow.Width);
                licenseWindow.Add(closeLicenseWindowButton);
                licenseWindow.Cursor.Position = new Point(0, 1);
                licenseWindow.Cursor.Print(
"Shattered World - Of Cybertech and Magitech\n\rCopyright(C) 2019  PurpleSharkBooks\n\r\n\rThis program is free software: you can redistribute it and / or modify\n\rit under the terms of the GNU General Public License as published by\n\rthe Free Software Foundation, either version 3 of the License, or\n\r(at your option) any later version.\n\rThis program is distributed in the hope that it will be useful,\n\rbut WITHOUT ANY WARRANTY; without even the implied warranty of\n\rMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\n\rGNU General Public License for more details.\n\rYou should have received a copy of the GNU General Public License\n\ralong with this program. If not, see < http://www.gnu.org/licenses/>.");
            }
            else
            {
                licenseWindow.Show();
            }
        }
        private void MapgenButton_Click(object sender, EventArgs e)
        {
            if (mgWindow == null)
            {
                mgWindow = UIFactory.CreateWindow(_screenWidth - 2, _screenHeight - 2, 1, 1);
                string winTitle = "World Generation Settings";
                mgWindow.Title = winTitle.Align(HorizontalAlignment.Center, mgWindow.Width);
                var mapWidthInput = new TextBox(10)
                {
                    Text = Data.World.GetMapWidth().ToString(),
                    IsNumeric = true,
                    IsCaretVisible = true,
                    Position = new Point("Map Width: ".Length + 2, 2),
                };
                var mapHeightInput = new TextBox(10)
                {
                    Text = Data.World.GetMapHeight().ToString(),
                    IsNumeric = true,
                    IsCaretVisible = true,
                    Position = new Point("Map Height: ".Length + 1, 3)
                };
                var mapSeedInput = new TextBox(10)
                {
                    Text = Data.World.GetSeed().ToString(),
                    IsNumeric = true,
                    IsCaretVisible = true,
                    Position = new Point("Map Seed: ".Length + 3, 4)
                };
                mapWidthInput.TextChanged += MapWidthInput_TextChanged;
                mapHeightInput.TextChanged += MapHeightInput_TextChanged;
                mapSeedInput.TextChanged += MapSeedInput_TextChanged;
                mgWindow.Add(closeMGWindowButton);
                mgWindow.Print(1, 2, "Map Width: ");
                mgWindow.Print(1, 3, "Map Height: ");
                mgWindow.Print(1, 4, "Map Seed: ");
                mgWindow.Add(mapWidthInput);
                mgWindow.Add(mapHeightInput);
                mgWindow.Add(mapSeedInput);
                Children.Add(mgWindow);
                mgWindow.Center();
            }
            else
            {
                mgWindow.Show();
            }
        }
        private void MapSeedInput_TextChanged(object sender, EventArgs e)
        {
            if (sender is TextBox tb)
            {
                System.Console.WriteLine("Seed: " + tb.Text);
                Data.World.SetSeed(int.Parse(tb.Text));
            }
        }
        private void MapHeightInput_TextChanged(object sender, EventArgs e)
        {
            if (sender is TextBox tb)
            {
                System.Console.WriteLine("X: " + tb.Text);
                Data.World.SetMapHeight(int.Parse(tb.Text));
            }
        }
        private void MapWidthInput_TextChanged(object sender, EventArgs e)
        {
            if (sender is TextBox tb)
            {
                System.Console.WriteLine("Y: " + tb.Text);
                Data.World.SetMapWidth(int.Parse(tb.Text));
            }
        }
        private void ChargenButton_Click(object sender, EventArgs e)
        {
            if (cgWindow == null)
            {
                cgWindow = UIFactory.CreateWindow(_screenWidth - 2, _screenHeight - 2, 1, 1);
                saveCharButton = UIFactory.CreateButton(16, "Save Character", cgWindow.Width - 16, cgWindow.Height - 1);
                PrintCharGenWindowTitle();
                cgWindow.Print(1, 1, "Traits");
                cgWindow.Print(2, _screenHeight - 5, "Description");
                cgWindow.Add(closeCGWindowButton);
                cgWindow.Add(saveCharButton);
                Children.Add(cgWindow);
                foreach (var item in listCBs)
                {
                    item.IsSelectedChanged += Item_IsSelectedChanged;
                    cgWindow.Add(item);
                }
                saveCharButton.Click += SaveCharButton_Click;
                cgWindow.Center();
            }
            else
            {
                cgWindow.Show();
            }
        }
        private void StartGameButton_Click(object sender, EventArgs e)
        {
            Data.World.OverworldMap = new Systems.OverworldMap(Data.World.GetMapWidth(), Data.World.GetMapHeight());
        }
        private void QuitGameButton_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
        private void SaveCharButton_Click(object sender, EventArgs e)
        {
            SWEntity pc = EntityFactory.NewEntityFromCGWindow(selectedComponents);
            foreach (var comp in pc.GetComponents<Defines.Components.TraitComponent>())
            {
                string test = $"{comp.Name} of {comp.Parent}: {comp.Target} with {comp.Effect}";
                System.Console.WriteLine(test);
            }
            System.Console.WriteLine($"G: {pc.Gender} S: {pc.Sexuality}");
            Data.World.SetPlayer(pc);
        }
        #endregion
    }
}

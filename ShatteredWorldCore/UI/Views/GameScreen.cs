﻿using GoRogue;
using SadConsole;

namespace ShatteredWorld.UI.Views
{
    public class GameScreen : ContainerConsole
    {
        public ScrollingConsole MapView;
        public ScrollingConsole InfoView;
        public ScrollingConsole MessageView;
        public GameScreen(int mapWidth,
                          int mapHeight,
                          int screenWidth,
                          int screenHeight)
        {
            MapView = new ScrollingConsole(mapWidth, mapHeight, Global.FontDefault, new Rectangle(0, 0, screenWidth, screenHeight - 6));
            InfoView = new ScrollingConsole(500, 100, new Rectangle(0, 0, screenWidth / 2, 5));
            MessageView = new ScrollingConsole(500, 100, new Rectangle(0, 0, screenWidth / 2, 5));
        }
    }
}

﻿using ShatteredWorld.Systems;
using System.Collections.Generic;

namespace ShatteredWorld.Defines
{
    class Manager
    {
        public List<BaseSystem> Systems { get; private set; } = new List<BaseSystem>();
        public List<SWEntity> Entities { get; private set; } = new List<SWEntity>();
        public void RegisterEntity(SWEntity entity)
        {
            if (Entities.Contains(entity))
                System.Diagnostics.Debug.WriteLine($"Entity of type {entity.GetType()} already registered.");
            else
                Entities.Add(entity);
        }
        public void RemoveEntity(SWEntity entity)
        {
            if (Entities.Contains(entity))
                Entities.Remove(entity);
            else
                System.Diagnostics.Debug.WriteLine($"Entity of type {entity.GetType()} not registered.");
        }
        public void RegisterSystem(BaseSystem system)
        {
            if (Systems.Contains(system))
                System.Diagnostics.Debug.WriteLine($"System of type {system.GetType()} already registered.");
            else
                Systems.Add(system);
        }
        public void RemoveSystem(BaseSystem system)
        {
            if (Systems.Contains(system))
                Systems.Remove(system);
            else
                System.Diagnostics.Debug.WriteLine($"System of type {system.GetType()} not registered.");
        }
        public delegate void CreateEntity(SWEntity ent); // doesn't feel right
    }
}

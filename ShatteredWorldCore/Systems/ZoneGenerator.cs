﻿using SharpNoise;
using SharpNoise.Builders;
using SharpNoise.Modules;
using System;

namespace ShatteredWorld.Systems
{
    public static class ZoneGenerator
    {
        public static int Seed { get; set; } = DateTime.Today.Millisecond;
        private static NoiseMap noiseMap = new NoiseMap();
        private static Cell voronoi = new Cell();
        private static Turbulence turbulence = new Turbulence
        {
            Source0 = voronoi,
            //Frequency = 1/8,
            //Power = 2
        };
        public static NoiseMap Generate(int width, int height)
        {
            var noiseMapBuilder = new PlaneNoiseMapBuilder
            {
                DestNoiseMap = noiseMap,
                SourceModule = turbulence
            };
            voronoi.Seed = Seed;
            voronoi.Type = Cell.CellType.Minkowsky;
            noiseMapBuilder.SetDestSize(width, height);
            noiseMapBuilder.SetBounds(-5, 5, 0, 10);
            noiseMapBuilder.Build();
            return noiseMap;
        }
    }
}

﻿using GoRogue;
using GoRogue.MapGeneration.Connectors;
using GoRogue.MapGeneration.Generators;
using GoRogue.MapViews;
using GoRogue.Random;
using Microsoft.Xna.Framework;
using SharpNoise;
using ShatteredWorld.Defines;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShatteredWorld.Systems
{
    public class OverworldMap : Map
    {
        public int Width { get; }
        public int Height { get; }
        private ArrayMap<bool> RawOverworldMap { get; set; }
        private const double HotSpotLowerLimit = 0.15;
        private const double MinPersp = 0.75;
        NoiseMap voronoiNoiseMap;
        NoiseMap rainNoise;
        public OverworldTile[] OverWorld { get; private set; }
        public int MinRivers { get; private set; } = 1;

        private PerlinGenerator masterNoise;
        private Biome calculatedBiome = new Biome();
        List<OverworldTile> RiverCandidates = new List<OverworldTile>();
        Utils.SimpleGraph graph;

        public OverworldMap(int width, int height)
        {
            SingletonRandom.DefaultRNG = new Troschuetz.Random.Generators.XorShift128Generator(Data.World.GetSeed());
            Width = width;
            Height = height;
            RawOverworldMap = new ArrayMap<bool>(Width, Height);
            voronoiNoiseMap = ZoneGenerator.Generate(width, height);
            OverWorld = new OverworldTile[Width * Height];
            masterNoise = new PerlinGenerator(Width, Height);
            masterNoise.ConfigureAll(4, 1, 0.25D, 2);
            CellularAutomataAreaGenerator.Generate(RawOverworldMap, null, 45, 8, 2);
            OrderedMapAreaConnector.Connect(RawOverworldMap, AdjacencyRule.EIGHT_WAY);
            DeadEndTrimmer.Trim(RawOverworldMap);
            RemoveLandmassByVoronoiNoise(RawOverworldMap);
            var perspnoise = masterNoise.Generate();
            rainNoise = perspnoise;
            NormalizePersp();
            BuildOverWorld(RawOverworldMap, OverWorld);
        }
        private void BuildOverWorld(ArrayMap<bool> rawOverworldMap, OverworldTile[] OverWorld)
        {
            AssignBiomes(rawOverworldMap, OverWorld);
            //var springs = FindRiverSprings(minRivers);
            DijkstraRivers(OverWorld, FindRiverSprings(MinRivers));
        }
        private void DijkstraRivers(OverworldTile[] OverWorld, List<OverworldTile> springs)
        {
            graph = new Utils.SimpleGraph("test", OverWorld, Width, Height);
            foreach (var tile in OverWorld)
            {
                graph.AddNode(tile.Position, tile.IsWalkable);
            }
            var startingpoints = new List<Coord>();
            foreach (var spring in springs)
            {
                foreach (var pos in graph.GetNeighborsOfNode(spring.Position))
                {
                    startingpoints.Add(pos);
                }
            }
            var nodes = new List<Coord>();
            foreach (var point in startingpoints)
            {
                var queue = new Queue<Coord>();
                graph.ResetGraph();
                queue.Enqueue(point);
                while (queue.Count() > 0)
                {
                    var current = queue.Dequeue();
                    //Console.WriteLine($"Visiting node {p} for start {point}...");
                    graph.VisitNode(current);
                    var neighbors = graph.GetWeightedNeighbors(current);
                    //Console.WriteLine(neighbors.First());
                    var item = neighbors.Last();
                    //neighbors.Remove(item.Key);

                    foreach (var neighbor in neighbors)
                    {
                        if (!nodes.Contains(neighbor.Value) && !graph.Visited(neighbor.Value))
                        {
                            item = neighbor;
                        }
                        else
                            break;
                    }

                    if (graph.Visited(item.Value) == false && OverWorld[Coord.ToIndex(current.X, current.Y, Width)].IsWalkable)// && item.Value != OverWorld[Coord.ToIndex(p.X, p.Y, Width)].Position)
                    {
                        queue.Enqueue(item.Value);
                        if (OverWorld[Coord.ToIndex(current.X, current.Y, Width)].RainNoise < item.Key)
                            nodes.Add(item.Value);
                        else
                            break;
                    }
                }
                foreach (var pos in nodes)
                {
                    var t = OverWorld[Coord.ToIndex(pos.X, pos.Y, Width)];
                    if (!t.IsWalkable)
                    {
                        t.Glyph = 178;
                        t.Background = Color.DarkBlue;
                        t.Foreground = Color.LightBlue;
                    }
                    else
                    {
                        t.IsWater = true;
                        t.Glyph = 247;
                        t.Background = Color.Blue;
                        t.Foreground = Color.LightBlue;
                    }
                }
                //foreach (var spring in springs)
                //{
                //    spring.Glyph = 'S';
                //    spring.Foreground = Color.Red;
                //    spring.Background = Color.Orange;
                //}
            }
        }

        private void AssignBiomes(ArrayMap<bool> rawOverworldMap, OverworldTile[] OverWorld)
        {
            foreach (var pos in rawOverworldMap.Positions())
            {
                OverworldTile tile = null;
                int glyph;
                calculatedBiome = DefineBiomeAt(pos);
                if (rawOverworldMap[pos]) // land tiles
                {

                    Color fgd = Color.White;
                    Color bgd = Color.DarkGray;
                    var fgdColdBiome = new Color(53, 79, 0);
                    var bkgColdBiome = new Color(141, 182, 0);
                    var bkgTempBiome = new Color(46, 110, 18);
                    var fgdTempBiome = new Color(00, 68, 00);
                    var bkgWarmBiome = new Color(45, 136, 45);
                    var fgdWarmBiome = new Color(0, 68, 0);
                    switch (calculatedBiome)
                    {
                        case Biome.Cold | Biome.Arid:
                            fgd = fgdColdBiome; //AzureMist
                            bgd = bkgColdBiome; // Apple Green
                            glyph = 44; // ','
                            break;
                        case Biome.Cold | Biome.SemiArid:
                            fgd = fgdColdBiome;
                            bgd = bkgColdBiome; // Apple Green
                            glyph = 46; // '.'
                            break;
                        case Biome.Cold | Biome.SemiHumid:
                            fgd = fgdColdBiome;
                            bgd = bkgColdBiome;
                            glyph = 15; // '☼'
                            break;
                        case Biome.Cold | Biome.Humid:
                            fgd = fgdColdBiome;
                            bgd = bkgColdBiome;
                            glyph = 42; // '*'
                            break;
                        case Biome.Temperate | Biome.Arid:
                            fgd = new Color(121, 182, 0);
                            bgd = bkgTempBiome;
                            glyph = 15; // '☼'
                            break;
                        case Biome.Temperate | Biome.SemiArid:
                            fgd = fgdTempBiome;
                            bgd = bkgTempBiome;
                            glyph = 6; // '♠'
                            break;
                        case Biome.Temperate | Biome.SemiHumid:
                            fgd = fgdTempBiome;
                            bgd = bkgTempBiome;
                            glyph = 6; // '♠'
                            break;
                        case Biome.Temperate | Biome.Humid:
                            fgd = fgdTempBiome;
                            bgd = bkgTempBiome;
                            glyph = 5; // '♣'
                            break;
                        case Biome.Warm | Biome.Arid:
                            fgd = fgdWarmBiome;
                            bgd = bkgWarmBiome;
                            glyph = 44; // ','
                            break;
                        case Biome.Warm | Biome.SemiArid:
                            fgd = fgdWarmBiome;
                            bgd = bkgWarmBiome;
                            glyph = 46; // '.'
                            break;
                        case Biome.Warm | Biome.SemiHumid:
                            fgd = fgdWarmBiome;
                            bgd = bkgWarmBiome;
                            glyph = 6; // '♠'
                            break;
                        case Biome.Warm | Biome.Humid:
                            fgd = fgdWarmBiome;
                            bgd = bkgWarmBiome;
                            glyph = 5; // '♣'
                            break;
                        default:
                            glyph = 63; // '?'
                            break;
                    }
                    tile = new OverworldTile($"Land - {glyph}", pos, glyph)
                    {
                        ZoneNoise = voronoiNoiseMap[pos.X, pos.Y],
                        RainNoise = rainNoise[pos.X, pos.Y],
                        Biome = calculatedBiome,
                        Foreground = fgd,
                        Background = bgd,
                        IsWalkable = true,
                        IsTransparent = true
                    };

                }
                else if (rawOverworldMap[pos] == false) // air tiles
                {
                    glyph = 126; // '~'
                    var bkg = new Color(93, 138, 168);
                    var fgd = Color.Aquamarine;
                    tile = new OverworldTile($"Air - {glyph}", pos, glyph)
                    {
                        ZoneNoise = voronoiNoiseMap[pos.X, pos.Y],
                        RainNoise = rainNoise[pos.X, pos.Y],
                        Biome = calculatedBiome,
                        Foreground = fgd,
                        Background = bkg,
                        IsWalkable = false,
                        IsTransparent = true
                    };
                }
                if (tile != null)
                {
                    OverWorld[Coord.ToIndex(pos.X, pos.Y, Width)] = tile;
                    //if (voronoiNoiseMap[pos.X, pos.Y] >= (voronoiNoiseMap.Data.Max() - HotSpotLowerLimit))
                    //{
                    //    if (rainNoise[pos.X, pos.Y] > 0.5)
                    //        riverCandidates.Add(OverWorld[Coord.ToIndex(pos.X, pos.Y, Width)]);
                    //}
                    bool candidate = rainNoise[pos.X, pos.Y] > MinPersp && (voronoiNoiseMap[pos.X, pos.Y] >= (voronoiNoiseMap.Data.Max() - HotSpotLowerLimit));
                    switch (tile.Biome)
                    {
                        case Biome.Cold | Biome.SemiHumid:
                            if (candidate)
                                RiverCandidates.Add(OverWorld[Coord.ToIndex(pos.X, pos.Y, Width)]);
                            break;
                        case Biome.Cold | Biome.Humid:
                            if (candidate)
                                RiverCandidates.Add(OverWorld[Coord.ToIndex(pos.X, pos.Y, Width)]);
                            break;
                        case Biome.Temperate | Biome.SemiHumid:
                            if (candidate)
                                RiverCandidates.Add(OverWorld[Coord.ToIndex(pos.X, pos.Y, Width)]);
                            break;
                        case Biome.Temperate | Biome.Humid:
                            if (candidate)
                                RiverCandidates.Add(OverWorld[Coord.ToIndex(pos.X, pos.Y, Width)]);
                            break;
                        case Biome.Warm | Biome.SemiHumid:
                            if (candidate)
                                RiverCandidates.Add(OverWorld[Coord.ToIndex(pos.X, pos.Y, Width)]);
                            break;
                        case Biome.Warm | Biome.Humid:
                            if (candidate)
                                RiverCandidates.Add(OverWorld[Coord.ToIndex(pos.X, pos.Y, Width)]);
                            break;
                        default:
                            break;
                    }
                    //if (tile.Biome == Biome.Humid || tile.Biome == Biome.SemiHumid)
                    //{
                    //    riverCandidates.Add(OverWorld[Coord.ToIndex(pos.X, pos.Y, Width)]);
                    //}
                }
                else
                    new Exception($"{tile.ID} is null.");
            }
        }
        private List<OverworldTile> FindRiverSprings(int minRivers)
        {
            List<OverworldTile> listPos = RiverCandidates;
            var rivers = new List<OverworldTile>();
            var riverstogenerate = SingletonRandom.DefaultRNG.Next(minRivers, listPos.Distinct().Count());
            //var riverstogenerate = Math.Max(minRivers, listPos.Distinct().Count());
            while (rivers.Count <= riverstogenerate)
            {
                int riverstart = SingletonRandom.DefaultRNG.Next(listPos.Count());
                var t = listPos.ElementAt(riverstart);
                if (!rivers.Contains(t) && (t.Position.X > 0 && t.Position.Y > 0) && !t.Name.Contains("Air"))
                {
                    //Console.WriteLine($"Using candidate {t}");
                    rivers.Add(t);
                }
                else
                {
                    //Console.WriteLine($"already used candidate {t.Name}@{t.Position}, picking new one");
                    listPos.Remove(t);
                    riverstart = SingletonRandom.DefaultRNG.Next(listPos.Count());
                    t = listPos.ElementAt(riverstart);
                    //Console.WriteLine($"Using candidate {t}");
                    if ((t.Position.X > 0 && t.Position.Y > 0) && !t.Name.Contains("Air"))
                        rivers.Add(t);
                }
            }
            //Console.WriteLine($"Total springs: {rivers.Count()}");
            return rivers;
        }

        private Biome DefineBiomeAt(Coord pos)
        {
            var biomePersp = new Biome();
            var biomeTemp = new Biome();
            if (-0.5f > voronoiNoiseMap[pos.X, pos.Y])
            {
                biomeTemp = Biome.Cold;
            }
            else if (voronoiNoiseMap[pos.X, pos.Y] > 0.5f)
            {
                biomeTemp = Biome.Warm;
            }
            else if ((-0.5f <= voronoiNoiseMap[pos.X, pos.Y]) && (voronoiNoiseMap[pos.X, pos.Y] <= 0.5f))
            {
                biomeTemp = Biome.Temperate;
            }
            if ((rainNoise[pos.X, pos.Y] >= 0.0f) && (rainNoise[pos.X, pos.Y] <= 0.2f))
            {
                biomePersp = Biome.Arid;
            }
            if ((0.2f < rainNoise[pos.X, pos.Y]) && (rainNoise[pos.X, pos.Y] <= 0.4f))
            {
                biomePersp = Biome.SemiArid;
            }
            if ((0.4f < rainNoise[pos.X, pos.Y]) && (rainNoise[pos.X, pos.Y] <= 0.6f))
            {
                biomePersp = Biome.SemiHumid;
            }
            if ((0.6f < rainNoise[pos.X, pos.Y]))
            {
                biomePersp = Biome.Humid;
            }
            return biomeTemp | biomePersp;
        }
        #region Data massaging
        private void NormalizePersp()
        {
            for (int i = 0; i < rainNoise.Width; i++)
            {
                for (int j = 0; j < rainNoise.Height; j++)
                {
                    rainNoise[i, j] = Math.Abs(rainNoise[i, j]);
                }
            }
        }
        private void RemoveLandmassByVoronoiNoise(ArrayMap<bool> rawOverworldMap)
        {
            bool remove = false;
            float remover = 0.0f;
            foreach (var p in rawOverworldMap.Positions())
            {
                if (rawOverworldMap[p] == false)
                {
                    remover = voronoiNoiseMap[p.X, p.Y];
                    remove = true;
                }
                if ((remove == true) && (voronoiNoiseMap[p.X, p.Y] == remover) && (rawOverworldMap[p] == true))
                    rawOverworldMap[p] = false;
            }
        }
        #endregion
        #region Helpers
        private byte ScaleColorAlpha(NoiseMap rainNoise, Coord pos)
        {
            var scaler = 255 * (1 - rainNoise[pos.X, pos.Y]);
            if (scaler < 127)
                scaler = 127;
            return (byte)scaler;
        }
        public void PrintUniqueNoise()
        {
            var distinctCellNoise = voronoiNoiseMap.Data.Distinct();
            //var distinctRainNoise = rainNoise.Data.Distinct();
            Console.WriteLine("Cell Noise:");
            foreach (var item in distinctCellNoise)
            {
                Console.WriteLine($"\tunique: {item}");
            }
            Console.WriteLine($"maxVal: {distinctCellNoise.Max()} minVal:{distinctCellNoise.Min()}");
        }
        public void PrintHotspots()
        {
            Console.WriteLine("Hotspots:");
            foreach (var item in RiverCandidates)
            {
                Console.WriteLine($"{item.Name} @ {item.Position}");
            }
        }
        #endregion
    }
}

﻿using SharpNoise;
using SharpNoise.Builders;
using SharpNoise.Modules;

namespace ShatteredWorld.Systems
{
    public class PerlinGenerator
    {
        public int Octaves { get; private set; }
        public double Frequency { get; private set; }
        public double Lacunarity { get; private set; }
        public double Persistence { get; private set; }
        public int Seed { get; private set; }
        public int Width { get; }
        public int Height { get; }
        private PlaneNoiseMapBuilder noiseMapBuilder;
        private NoiseMap noiseMap = new NoiseMap();
        public PerlinGenerator(int width,
                               int height,
                               int seed = 0)
        {
            Width = width;
            Height = height;
            Seed = seed;
            var perlinGen = new Perlin
            {
                Seed = Seed
            };
            noiseMapBuilder = new PlaneNoiseMapBuilder
            {
                DestNoiseMap = noiseMap,
                SourceModule = perlinGen
            };
        }
        public PerlinGenerator(int octaves,
                               double frequency,
                               double lacunarity,
                               double persistence,
                               int width,
                               int height,
                               int seed = 0)
        {
            Octaves = octaves;
            Frequency = frequency;
            Lacunarity = lacunarity;
            Persistence = persistence;
            Seed = seed;
            Width = width;
            Height = height;
            var perlinGen = new Perlin
            {
                OctaveCount = Octaves,
                Frequency = Frequency,
                Lacunarity = Lacunarity,
                Persistence = Persistence,
                Seed = Seed
            };
            noiseMapBuilder = new PlaneNoiseMapBuilder
            {
                DestNoiseMap = noiseMap,
                SourceModule = perlinGen
            };
        }
        public void Build()
        {
            if ((noiseMap == null) || noiseMap.IsEmpty)
            {
                noiseMapBuilder.SetDestSize(Width, Height);
                noiseMapBuilder.SetBounds(Width / 10, Width, Height / 10, Height);
                noiseMapBuilder.Build();
            }
        }
        public NoiseMap Generate()
        {
            if ((noiseMap == null) || noiseMap.IsEmpty)
                Build();
            return noiseMap;
        }
        public NoiseMap GetNoiseMap()
        {
            return noiseMap;
        }
        public void ConfigureAll(int octaves, double frequency, double lacunarity, double persistence, int seed = 0)
        {
            Octaves = octaves;
            Frequency = frequency;
            Lacunarity = lacunarity;
            Persistence = persistence;
            Seed = seed;
        }
    }
}

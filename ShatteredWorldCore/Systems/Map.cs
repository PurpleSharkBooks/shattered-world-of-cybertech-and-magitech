﻿namespace ShatteredWorld.Systems
{
    public abstract class Map
    {
        private int _width;
        private int _height;
        public Map(int width, int height)
        {
            _width = width;
            _height = height;

        }
        public Map()
        {
            _width = Data.World.GetMapWidth();
            _height = Data.World.GetMapHeight();
        }
    }
}

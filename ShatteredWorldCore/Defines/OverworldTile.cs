﻿using GoRogue;
using Microsoft.Xna.Framework;

namespace ShatteredWorld.Defines
{
    public class OverworldTile : BaseTile
    {
        //public string Name { get; }
        public float ZoneNoise { get; set; }
        public float RainNoise { get; set; }
        public Biome Biome { get; set; }
        public bool IsWater { get; set; }

        public OverworldTile(string name,
                             Coord position,
                             int glyph,
                             bool isWalkable,
                             bool isTransparent,
                             float zoneNoise,
                             float rainNoise,
                             Biome biome,
                             Color fore,
                             Color back) : base(name, position, glyph, isWalkable, isTransparent, fore, back)
        {
            ZoneNoise = zoneNoise;
            RainNoise = rainNoise;
            Biome = biome;
            Foreground = fore;
            Background = back;
            Glyph = glyph;
            IsWater = false;
        }
        public OverworldTile(string name,
                             Coord pos,
                             int glyph) : base(name, pos, glyph, true, true, Color.White, Color.Black)
        {
            IsWater = false;
        }

    }
}

﻿using GoRogue;
using GoRogue.GameFramework;
using Microsoft.Xna.Framework;
using SadConsole;
using System;
using System.Collections.Generic;

namespace ShatteredWorld.Defines
{
    public abstract class BaseTile : Cell, IGameObject
    {
        IGameObject gameObject;
        public string Name { get; set; }

        public Map CurrentMap => gameObject.CurrentMap;

        public bool IsStatic => gameObject.IsStatic;

        public bool IsTransparent { get => gameObject.IsTransparent; set => gameObject.IsTransparent = value; }
        public bool IsWalkable { get => gameObject.IsWalkable; set => gameObject.IsWalkable = value; }
        public Coord Position { get => gameObject.Position; set => gameObject.Position = value; }

        public uint ID => gameObject.ID;

        public int Layer => gameObject.Layer;

        protected BaseTile(string name,
                           Coord position,
                           int glyph,
                           bool isWalkable,
                           bool isTransparent,
                           Color fore,
                           Color back) : base(fore, back, glyph)
        {
            gameObject = new GameObject(position, 0, null);
            Name = name;
            //Position = position;
            IsWalkable = isWalkable;
            IsTransparent = isTransparent;
            Foreground = fore;
            Background = back;
            Glyph = glyph;
        }

        public event EventHandler<ItemMovedEventArgs<IGameObject>> Moved
        {
            add
            {
                gameObject.Moved += value;
            }

            remove
            {
                gameObject.Moved -= value;
            }
        }

        public bool MoveIn(Direction direction)
        {
            return gameObject.MoveIn(direction);
        }

        public void OnMapChanged(Map newMap)
        {
            gameObject.OnMapChanged(newMap);
        }

        public void AddComponent(object component)
        {
            gameObject.AddComponent(component);
        }

        public T GetComponent<T>()
        {
            return gameObject.GetComponent<T>();
        }

        public IEnumerable<T> GetComponents<T>()
        {
            return gameObject.GetComponents<T>();
        }

        public bool HasComponent(Type componentType)
        {
            return gameObject.HasComponent(componentType);
        }

        public bool HasComponent<T>()
        {
            return gameObject.HasComponent<T>();
        }

        public bool HasComponents(params Type[] componentTypes)
        {
            return gameObject.HasComponents(componentTypes);
        }

        public void RemoveComponent(object component)
        {
            gameObject.RemoveComponent(component);
        }

        public void RemoveComponents(params object[] components)
        {
            gameObject.RemoveComponents(components);
        }
    }
}

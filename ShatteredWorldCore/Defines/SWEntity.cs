﻿using GoRogue;
using GoRogue.GameFramework;
using System;
using System.Collections.Generic;

namespace ShatteredWorld.Defines
{
    public class SWEntity : SadConsole.Entities.Entity, IGameObject
    {
        internal IGameObject gameObject;
        public string Gender { get; set; } = "NB";
        public string Sexuality { get; set; } = "Pan";
        public SWEntity(int x, int y) : base(x, y)
        {
        }
        public void InitializeAsParent(Coord position, int layer)
        {
            if (gameObject == null)
                gameObject = new GameObject(position, layer, null);
        }
        public void InitializeAsChild(Coord position, int layer, GameObject parent)
        {
            if (gameObject == null)
                gameObject = new GameObject(position, layer, parent);
        }
        #region autoimplemented IGameObject through gameObject
        public Map CurrentMap => gameObject.CurrentMap;

        public bool IsStatic => gameObject.IsStatic;

        public bool IsTransparent { get => gameObject.IsTransparent; set => gameObject.IsTransparent = value; }
        public bool IsWalkable { get => gameObject.IsWalkable; set => gameObject.IsWalkable = value; }

        public uint ID => gameObject.ID;

        public int Layer => gameObject.Layer;

        Coord IGameObject.Position { get => gameObject.Position; set => gameObject.Position = value; }

        event EventHandler<ItemMovedEventArgs<IGameObject>> IGameObject.Moved
        {
            add
            {
                gameObject.Moved += value;
            }

            remove
            {
                gameObject.Moved -= value;
            }
        }

        public void AddComponent(object component)
        {
            gameObject.AddComponent(component);
        }

        public T GetComponent<T>()
        {
            return gameObject.GetComponent<T>();
        }

        public IEnumerable<T> GetComponents<T>()
        {
            return gameObject.GetComponents<T>();
        }

        public bool HasComponent(Type componentType)
        {
            return gameObject.HasComponent(componentType);
        }

        public bool HasComponent<T>()
        {
            return gameObject.HasComponent<T>();
        }

        public bool HasComponents(params Type[] componentTypes)
        {
            return gameObject.HasComponents(componentTypes);
        }

        public bool MoveIn(Direction direction)
        {
            return gameObject.MoveIn(direction);
        }

        public void OnMapChanged(Map newMap)
        {
            gameObject.OnMapChanged(newMap);
        }

        public void RemoveComponent(object component)
        {
            gameObject.RemoveComponent(component);
        }

        public void RemoveComponents(params object[] components)
        {
            gameObject.RemoveComponents(components);
        }
        #endregion
    }
}

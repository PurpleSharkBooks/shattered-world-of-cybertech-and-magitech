﻿namespace ShatteredWorld.Defines
{
    public enum Targets
    {
        None = 1,
        Self,
        SingleBeing
    }
}

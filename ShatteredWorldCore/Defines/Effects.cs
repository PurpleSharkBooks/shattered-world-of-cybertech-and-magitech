﻿namespace ShatteredWorld.Defines
{
    public enum Effects
    {
        None = 1,
        BasicHealth,
        BasicWill,
        BonusHealth,
        BonusWill,
        BonusStability,
        BonusAction,
        StunAttack,
        CombatBonus
    }
}

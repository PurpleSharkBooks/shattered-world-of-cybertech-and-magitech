﻿using System;

namespace ShatteredWorld.Defines
{
    [Flags]
    public enum Biome
    {
        Temperate = 1,
        Cold = 2,
        Warm = 4,
        SemiArid = 8,
        SemiHumid = 16,
        Arid = 32,
        Humid = 64
    }
}

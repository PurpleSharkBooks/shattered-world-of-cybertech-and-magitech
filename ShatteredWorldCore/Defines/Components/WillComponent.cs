﻿using GoRogue.GameFramework;
using GoRogue.GameFramework.Components;

namespace ShatteredWorld.Defines.Components
{
    class Component : IGameObjectComponent
    {
        public IGameObject Parent { get; set; }
    }
}

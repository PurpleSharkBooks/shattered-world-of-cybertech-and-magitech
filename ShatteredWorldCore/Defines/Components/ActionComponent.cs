﻿using GoRogue.GameFramework;
using GoRogue.GameFramework.Components;

namespace ShatteredWorld.Defines.Components
{
    public class ActionComponent : IGameObjectComponent
    {
        public IGameObject Parent { get; set; }
        //public GoRogue.GameFramework.IGameObject TargetComponent { get; set; }
        public int Actions { get; set; }
    }
}

﻿using GoRogue.GameFramework;
using GoRogue.GameFramework.Components;

namespace ShatteredWorld.Defines.Components
{
    class StabilityComponent : IGameObjectComponent
    {
        public int MaxStability { get; set; }
        public int CurrentStability { get; set; }
        public IGameObject Parent { get; set; }
    }
}

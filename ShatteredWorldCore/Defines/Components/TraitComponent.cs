﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ShatteredWorld.Defines.Components
{
    public class TraitComponent : GoRogue.GameFramework.Components.IGameObjectComponent
    {
        public GoRogue.GameFramework.IGameObject Parent { get; set; }
        //public GoRogue.GameFramework.IGameObject TargetComponent { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Cost { get; set; }
        public string Category { get; set; } = "None";

        [JsonProperty("Effect")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Effects Effect { get; set; } = new Effects();
        [JsonProperty("Target")]
        [JsonConverter(typeof(StringEnumConverter))]
        public Targets Target { get; set; } = new Targets();


        public TraitComponent(string name,
                     string description,
                     int cost,
                     Effects effect,
                     Targets target)
        {
            Name = name;
            Description = description;
            Cost = cost;
            Effect = effect;
            Target = target;
        }
        [Newtonsoft.Json.JsonConstructor]
        public TraitComponent(string name,
                     string description,
                     int cost,
                     string category,
                     Effects effect,
                     Targets target)
        {
            Name = name;
            Description = description;
            Category = category;
            Cost = cost;
            Effect = effect;
            Target = target;
        }
    }
}

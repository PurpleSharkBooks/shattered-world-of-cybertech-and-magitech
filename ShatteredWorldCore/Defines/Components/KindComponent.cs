﻿using System.Collections.Generic;

namespace ShatteredWorld.Defines.Components
{
    /// <summary>
    /// Represents the kind of character to play.
    /// </summary>
    public class KindComponent : GoRogue.GameFramework.Components.IGameObjectComponent
    {
        public GoRogue.GameFramework.IGameObject Parent { get; set; }
        public string Name { get; private set; }
        public int Actions { get; set; }
        public string BaseAttack { get; set; }
        public string BaseDefense { get; set; }
        public string Description { get; set; }
        public int Cost { get; set; }
        public List<string> Components { get; set; }
        /// <summary>
        /// Creates an instance of the Kind component.
        /// </summary>
        /// <param name="name">The friendly name (from SadConsole.Entities.Entity)</param>
        /// <param name="actions">The number of actions per turn available to the character.</param>
        /// <param name="baseAttack">The unmodified number of dice the character can roll on attack (for GoRogue.DiceNotation).</param>
        /// <param name="baseDefense">The unmodified number of dice the character can roll on defense (for GoRogue.DiceNotation).</param>
        /// <param name="cost">The cost in build points.</param>
        /// <param name="description">A description of the Kind (can be an empty string, but that's not very helpful)</param>
        public KindComponent(string name,
                    int actions,
                    string baseAttack,
                    string baseDefense,
                    int cost,
                    string description,
                    List<string> components)// : base(Microsoft.Xna.Framework.Color.White, Microsoft.Xna.Framework.Color.Transparent, '@')
        {
            Name = name;
            Actions = actions;
            BaseAttack = baseAttack;
            BaseDefense = baseDefense;
            Cost = cost;
            Description = description;
            Components = components;
        }
    }
}

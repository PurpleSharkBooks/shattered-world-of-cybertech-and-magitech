﻿using GoRogue.GameFramework;
using GoRogue.GameFramework.Components;

namespace ShatteredWorld.Defines.Components
{
    class WillComponent : IGameObjectComponent
    {
        public double MaxWill { get; set; }
        public double CurrentWill { get; set; }
        public IGameObject Parent { get; set; }
    }
}

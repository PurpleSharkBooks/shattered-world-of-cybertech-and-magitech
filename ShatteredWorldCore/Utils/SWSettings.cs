using System;

namespace ShatteredWorld.Utils
{
    public class SWSettings
    {
        public int ResolutionWidth { get; set; }
        public int ResolutionHeight { get; set; }
        public int DefaultWorldMapWidth { get; set; }
        public int DefaultWorldMapHeight { get; set; }
        public bool FirstRun { get; set; }
        public void Populate()
        {

        }
    }
}
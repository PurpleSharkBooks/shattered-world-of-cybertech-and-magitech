﻿using ShatteredWorld.Defines.Components;
using ShatteredWorld.Utils;
using System;
using System.Collections.Generic;
using System.IO;

namespace ShatteredWorld.Utils
{
    public class FileLoader
    {
        internal static string ConfigFilePath { get; private set; } = Environment.CurrentDirectory + "/GameData/settings.json";
        internal string dataPath = Environment.CurrentDirectory + "/GameData/Components/";
        internal IEnumerable<string> ComponentFiles { get; private set; }
        public static SadConsole.Ansi.Document TitleScreen { get; internal set; }

        public FileLoader()
        {
            TitleScreen = new SadConsole.Ansi.Document("./GameData/Assets/ansi-title.ans");
            ComponentFiles = Directory.EnumerateFiles(dataPath, "*.json", SearchOption.AllDirectories);
        }
        /// <summary>
        /// Loads and deserializes the components defining a trait usable by Entities.
        /// Note that this documents the schema Traits have to follow in the code itself!
        /// </summary>
        //internal Dictionary<string, TraitComponent> LoadTraitComponents()
        //{
        //    var Traits = new Dictionary<string, TraitComponent>();
        //    foreach (var file in TraitComponentFiles)
        //    {
        //        var d = Newtonsoft.Json.JsonConvert.DeserializeObject<TraitComponent>(File.ReadAllText(file));
        //        Traits.Add(Path.GetFileNameWithoutExtension(file), d);
        //        //System.Diagnostics.Debug.WriteLine( $"[{d.Cost}] {d.Name} {{{d.Category}}} - {d.Description} ({d.Effect})" );
        //    }
        //    return Traits;
        //}

        /// <summary>
        /// Loads and deserializes the components defining a kind of character a player can play as.
        /// Note that this documents the schema Kind components have to follow in the code itself!
        /// </summary>
        internal Dictionary<string, TraitComponent> LoadComponents()
        {
            var Traits = new Dictionary<string, TraitComponent>();
            foreach (var file in ComponentFiles)
            {
                var d = Newtonsoft.Json.JsonConvert.DeserializeObject<TraitComponent>(File.ReadAllText(file));
                Traits.Add(Path.GetFileNameWithoutExtension(file), d);
            }
            return Traits;
        }
        /// <summary>
        /// Load Settings from a (hardcoded!) JSON file location.
        /// </summary>
        /// <returns>An SWSettings object</returns>
        internal Utils.SWSettings LoadSettings()
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Utils.SWSettings>(File.ReadAllText(ConfigFilePath));
        }
    }
}

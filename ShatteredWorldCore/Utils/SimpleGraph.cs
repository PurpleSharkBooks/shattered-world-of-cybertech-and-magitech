﻿using GoRogue;
using ShatteredWorld.Defines;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShatteredWorld.Utils
{
    public class SimpleGraph
    {
        internal class Vertex
        {
            public Coord Pos { get; }
            public bool Visited { get; set; } = false;
            public Coord Visitor { get; set; }
            public List<Edge> Edges { get; set; }
            public bool IsWalkable { get; set; }
            public Vertex(Coord pos, List<Edge> edges)
            {
                Pos = pos;
                Edges = edges;
            }
            public Vertex(Coord pos, bool isWalkable, List<Edge> edges)
            {
                Pos = pos;
                Edges = edges;
                IsWalkable = isWalkable;
            }
        }
        internal class Edge
        {
            public Coord Pos { get; }
            public float PerspNoise { get; }
            public Edge(Coord pos, float rainNoise)
            {
                Pos = pos;
                PerspNoise = rainNoise;
            }
        }
        private Dictionary<Vertex, List<Edge>> Edges = new Dictionary<Vertex, List<Edge>>();
        private List<Vertex> Nodes = new List<Vertex>();
        public string Name { get; private set; }
        private readonly OverworldTile[] tiles;
        private int _width;
        private int _height;
        private readonly Coord[] dirs = { (1, 0), (0, 1), (-1, 0), (0, -1) }; // N, E, S, W
        public SimpleGraph(string name, OverworldTile[] tiles, int width, int height)
        {
            Name = name;
            this.tiles = tiles;
            _width = width;
            _height = height;
        }
        public void AddNode(Coord pos)
        {
            List<Edge> neighbors = FindNeigbors(pos);
            Vertex vert = new Vertex(pos, tiles[Coord.ToIndex(pos.X, pos.Y, _width)].IsWalkable, neighbors);
            InsertNodesInGraph(neighbors, vert);
        }
        public void AddNode(Coord pos, bool walkable)
        {
            List<Edge> neighbors = FindNeigbors(pos);
            Vertex vert = new Vertex(pos, walkable, neighbors);
            InsertNodesInGraph(neighbors, vert);
        }
        public void VisitNode(Coord pos)
        {
            var vert = GetNode(pos);
            vert.Visited = true;
        }
        public bool Visited(Coord pos)
        {
            return GetNode(pos).Visited;
        }
        public void ResetGraph()
        {
            foreach (var item in Nodes)
            {
                item.Visited = false;
            }
        }
        public bool RemoveNode(Coord position)
        {
            var vert = GetNode(position);
            if (vert != null)
            {
                Edges.Remove(vert);
                Nodes.Remove(vert);
                return true;
            }
            else
                return false;
        }
        public List<Coord> GetNeighborsOfNode(Coord position)
        {
            var pos = Nodes.Find(e => e.Pos == position);
            List<Coord> neighbors = new List<Coord>();
            foreach (var p in Edges[pos])
            {
                neighbors.Add(p.Pos);
            }
            return neighbors;
        }
        public SortedList<float, Coord> GetWeightedNeighbors(Coord position)
        {
            var pos = Nodes.Find(e => e.Pos == position);
            SortedList<float, Coord> neighbors = new SortedList<float, Coord>();
            foreach (var p in Edges[pos])
            {
                neighbors.Add(p.PerspNoise * 10, p.Pos);
            }
            //neighbors.OrderBy(e => e.Item2).ToList();
            return neighbors;
        }
        public void PrintGraph()
        {
            foreach (var node in Nodes)
            {
                string result = "";
                foreach (var item in Edges[node])
                {
                    result += $"{item.Pos} ({item.PerspNoise}) ";
                }
                Console.WriteLine($"{node} has {result}");
            }
        }
        private Vertex GetNode(Coord pos)
        {
            return Nodes.Find(e => e.Pos == pos);
        }
        private List<Edge> FindNeigbors(Coord pos)
        {
            var list = new List<Edge>();
            foreach (var dir in dirs)
            {
                if (IsWithinBounds(pos + dir))
                {
                    var tile = tiles[Coord.ToIndex(pos.X + dir.X, pos.Y + dir.Y, _width)];
                    list.Add(new Edge(tile.Position, tile.RainNoise));
                }
            }
            return list;
        }
        private bool IsWithinBounds(Coord pos)
        {
            return (0 <= pos.X)
                   && (pos.X < _width)
                   && (0 <= pos.Y)
                   && (pos.Y < _height);
        }
        private void InsertNodesInGraph(List<Edge> neighbors, Vertex vert)
        {
            Nodes.Add(vert);
            Edges.TryAdd(vert, neighbors);
        }
    }
}

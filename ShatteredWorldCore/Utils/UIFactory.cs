﻿using Microsoft.Xna.Framework;
using SadConsole;
using SadConsole.Controls;

namespace ShatteredWorld.Utils
{
    public static class UIFactory
    {
        /// <summary>
        /// Generates a new SadConsole Window on demand.
        /// </summary>
        /// <param name="width">The width of the Window.</param>
        /// <param name="height">The height of the Window.</param>
        /// <param name="posx">The Window's X coordinate in a Console.</param>
        /// <param name="posy">The Window's Y coordinate in a Console.</param>
        /// <returns>SadConsole.Window</returns>
        public static Window CreateWindow(int width, int height, int posx, int posy)
        {
            return new Window(width, height)
            {
                IsModalDefault = true,
                IsVisible = true,
                Position = new Point(posx, posy)
            };
        }
        /// <summary>
        /// Generates a new SadConsole Button on demand.
        /// </summary>
        /// <param name="width">The width of the Button.</param>
        /// <param name="text">The label text.</param>
        /// <param name="posx">The Button's X coordinate in a Console.</param>
        /// <param name="posy">The Button's Y coordinate in a Console.</param>
        /// <param name="height">The height of the Button, defaults to 1.</param>
        /// <returns>SadConsole.Controls.Button</returns>
        public static Button CreateButton(int width, string text, int posx, int posy, int height = 1)
        {
            return new Button(width, height)
            {
                IsVisible = true,
                UseMouse = true,
                Position = new Point(posx, posy),
                Text = text
            };
        }
    }
}

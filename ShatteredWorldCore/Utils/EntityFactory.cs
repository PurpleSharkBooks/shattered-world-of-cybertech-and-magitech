﻿using ShatteredWorld.Data;
using ShatteredWorld.Defines;
using ShatteredWorld.Defines.Components;
using System.Collections.Generic;

namespace ShatteredWorld.Utils
{
    public static class EntityFactory
    {
        public static SWEntity NewEntityFromCGWindow(List<string> components)
        {
            var ent = new SWEntity(1, 1);
            ent.InitializeAsParent((1, 1), 1);
            foreach (var item in components)
            {
                var component = new TraitComponent(Entities.Traits[item].Name, Entities.Traits[item].Description,
                                                   Entities.Traits[item].Cost, Entities.Traits[item].Effect,
                                                   Entities.Traits[item].Target);
                ent.AddComponent(component);
                if (component.Category == "Sexuality")
                    ent.Sexuality = component.Name;
            }

            return ent;
        }
    }
}

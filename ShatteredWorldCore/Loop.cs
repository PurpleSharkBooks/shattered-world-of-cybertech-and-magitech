﻿using Microsoft.Xna.Framework;
using SadConsole;
using ShatteredWorld.Systems;
using ShatteredWorld.Utils;
using System;

namespace ShatteredWorld
{
    class Loop
    {
        public const int ScreenWidth = 110;
        public const int ScreenHeight = 55;
        public const int MapWidth = 1000;
        public const int MapHeight = 1000;
        public static UserInterface GameView;
        public static Utils.FileLoader FileLoader;
        public static Utils.SWSettings GameSettings;
        //public static SWData World;
        public enum MenuStates
        {
            MainMenu,
            OptionsMenu,
            InGame
        }
        public static bool InMainMenu { get; set; } = true;
        internal static MenuStates MenuState { get; set; }
        static FontMaster fontMaster;

        static void Main(string[] args)
        {
            // Setup the engine and create the main window.
            SadConsole.Game.Create("./GameData/Assets/Fonts/Cheepicus12.font", ScreenWidth, ScreenHeight);
            // Hook the start event so we can add consoles to the system.
            SadConsole.Game.OnInitialize = Init;

            // Hook update event.
            SadConsole.Game.OnUpdate = Update;

            // Screen resize.
            ((SadConsole.Game)SadConsole.Game.Instance).WindowResized += Program_WindowResized;

            // Start the game.
            SadConsole.Game.Instance.Run();
            //GameView.IngameView(ScreenWidth, ScreenHeight);
            SadConsole.Game.Instance.Dispose();
        }

        private static void Update(GameTime time)
        {
            switch (MenuState)
            {
                case MenuStates.MainMenu:
                    Global.CurrentScreen = GameView.StartScreen;
                    break;
                case MenuStates.InGame:
                    //TODO: Gameloop here
                    break;
            }
        }
        private static void Init()
        {
            System.Console.WriteLine(
                @"Shattered World - Of Cybertech and Magitech
    Copyright(C) 2019  PurpleSharkBooks

    This program is free software: you can redistribute it and / or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see < http://www.gnu.org/licenses/>.");
            FileLoader = new Utils.FileLoader();
            GameSettings = FileLoader.LoadSettings();
            SadConsole.Game.Instance.Window.IsBorderless = false;
            fontMaster = SadConsole.Global.LoadFont("./GameData/Assets/Fonts/Cheepicus12.font");
            Global.FontDefault = fontMaster.GetFont(Font.FontSizes.One);
            GetSystemInfo();
            //System.Console.WriteLine($"{SadConsole.Game.Instance.Window.IsBorderless}");
            Data.Entities.Traits = FileLoader.LoadComponents();
            Settings.ResizeMode = Settings.WindowResizeOptions.None;
            GameView = new UserInterface(ScreenWidth, ScreenHeight, MapWidth, MapHeight);
            GameView.Font = fontMaster.GetFont(Font.FontSizes.One);
            Global.CurrentScreen.Font = fontMaster.GetFont(Font.FontSizes.One);
            Global.CurrentScreen.Resize(110, 55, false);
            Global.CurrentScreen = GameView;
        }
        private static void GetSystemInfo()
        {
            System.Console.WriteLine($"Config file is {FileLoader.ConfigFilePath}");
            System.Console.WriteLine("Available graphics modes:");
            foreach (var mode in Global.GraphicsDevice.Adapter.SupportedDisplayModes)
            {
                System.Console.WriteLine($"{mode.Width}x{mode.Height} ({mode.AspectRatio})");
            }
            System.Console.WriteLine("Screen information:");
            System.Console.WriteLine($"Current Resolution :{Global.GraphicsDevice.Viewport.Width}x{Global.GraphicsDevice.Viewport.Height}");
            System.Console.WriteLine($"Configured Resolution :{GameSettings.ResolutionWidth}x{GameSettings.ResolutionHeight}");
            System.Console.WriteLine($"Default World Map size: {GameSettings.DefaultWorldMapWidth}x{GameSettings.DefaultWorldMapHeight}");
            System.Console.WriteLine($"First Run? {GameSettings.FirstRun}");
        }

        private static void Program_WindowResized(object sender, EventArgs e)
        {
            int newWindowWidth = Global.WindowWidth / Global.CurrentScreen.Font.Size.X;
            int newWindowHeight = Global.WindowHeight / Global.CurrentScreen.Font.Size.Y;
            Global.CurrentScreen.Resize(width: newWindowWidth, height: newWindowHeight, clear: false);
            foreach (var child in Global.CurrentScreen.Children)
            {
                if (child is ControlsConsole c)
                {
                    c.Resize(width: newWindowWidth, height: newWindowHeight, clear: true);
                    c.ViewPort = new Rectangle(0, 0, newWindowWidth, newWindowHeight);
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("child is not resizable (yet)");
                }
            }
        }
    }
}
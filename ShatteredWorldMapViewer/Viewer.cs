﻿using ShatteredWorld.Defines;
using ShatteredWorld.Systems;
using ShatteredWorld.Utils;

namespace ShatterWorldMapViewer
{
    class Viewer
    {
        static SadConsole.ScrollingConsole root;
        static int width = 120, height = 45;
        static void Main(string[] args)
        {
            // Setup the engine and create the main window.
            SadConsole.Game.Create(width, height);

            // Hook the start event so we can add consoles to the system.
            SadConsole.Game.OnInitialize = Init;

            // Hook update event.
            //SadConsole.Game.OnUpdate = Update;

            // Screen resize.
            //((SadConsole.Game)SadConsole.Game.Instance).WindowResized += Program_WindowResized;

            // Start the game.
            SadConsole.Game.Instance.Run();
            //GameView.IngameView(ScreenWidth, ScreenHeight);
            SadConsole.Game.Instance.Dispose();
        }

        private static void Init()
        {
            //ShatteredWorld.Data.World.SetSeed();
            var OverWorld = new OverworldMap(width, height);
            root = new SadConsole.ScrollingConsole(width,
                                        height,
                                        SadConsole.Global.FontDefault,
                                        new Microsoft.Xna.Framework.Rectangle(0, 0, width, height),
                                        OverWorld.OverWorld);
            root.MouseButtonClicked += Root_MouseButtonClicked;
            SadConsole.Global.CurrentScreen = root;
        }
        private static void Root_MouseButtonClicked(object sender, SadConsole.Input.MouseEventArgs e)
        {
            if (e.MouseState.Cell is OverworldTile cell)
            {
                System.Console.WriteLine($"Cell {cell.Name} at ({cell.Position.X}|{cell.Position.Y}):\n" +
                    $"\tNoise value: {cell.ZoneNoise}\n" +
                    $"\tPersp. noise: {cell.RainNoise}\n" +
                    $"\tBiome: {cell.Biome}\n" +
                    $"\tWater: {cell.IsWater}\n" +
                    $"\tColor: {cell.Foreground} : {cell.Background}");
            }
        }
    }
}
